package good.posture.sensors.telegram;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Observable;
import java.util.Observer;

@SuppressWarnings("deprecation")
public class NotifyTelegram implements Observer {
	
	private static final String BOT_TOKEN = "5611692396:AAE8V-dQLcHabBS9JW9wNXeK7eXBXnEGUO0";
	private static final String CHAT_ID = "1578739651";

	@Override
	public void update(Observable o, Object arg) {
		sendNotify((String) arg);
	}
	
	private void sendNotify(String message) {
		URL obj;
		try {
			obj = new URL("https://api.telegram.org/bot" + BOT_TOKEN + "/sendMessage?chat_id=" + CHAT_ID + "&text="
					+ message);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			int responseCode = con.getResponseCode();
			System.out.println("GET Response Code :: " + responseCode);
			if (responseCode == HttpURLConnection.HTTP_OK) { // success
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				System.out.println(response.toString());
			} else {
				System.out.println("GET request not worked");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
